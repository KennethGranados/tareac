﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculadora.Models
{
    class CalculoClass
    {
        String Resp = "";
        decimal cont, result;
        public String getPrimo(decimal numeva)
        {
            
            if (numeva == 1)
            {
                Resp = "No es un numero primo";
            }
            else
            {
                for (decimal i = 1; i < numeva; i++)
                {
                    if ((numeva % i) == 0)
                    {
                        cont++;
                    }
                }
                if (cont == 1)
                {
                    Resp = "El número ingresado es primo";

                }
                else
                {
                    Resp = "El número ingresado no es primo";
                }
            }
            return Resp;
        }

        public decimal getFactorial(decimal numeva)
        {
            decimal inicio = 1;
            for (decimal i = 1; i <= numeva; i++)
            {
                inicio = inicio * i;

            }
            result = inicio;
            return result;
        }
         public decimal getFibonacci(decimal numeva)
        {
            decimal a = 1, b = 1, c = 0;
            for (decimal i = 0; i <= numeva; i++)
            {
                if (i < numeva)
                {
                    c = a + b;
                    a = b;
                    b = c;
                }
            }
            result = a;
            return result;
        }
        public String getSerieFibonacci(decimal n)
        {
            decimal s = 1, u = 1, p;

            decimal i;
            switch (n)
            {
                case 0:
                    Resp = "0";
                    break;
                case 1:
                    Resp = "0, 1";
                    break;
                case 2:
                    Resp = "0, 1, 1";
                    break;
                default:
                    Resp = "0, 1, 1";
                    for (i = 3; i <= n; i++)
                    {
                        p = u;
                        u = s;
                        s = s + p;
                        Resp += ", " + String.Concat(s);
                    }
                    break;
            }
            return Resp;
        }
        public String getResp()
        {
            return Resp;
        }
        public decimal getResult()
        {
            return result;
        }
    }
}
