﻿using Calculadora.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Calculadora.Views
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        int basicOperator = 0;
        int specialOperator = 0;
        bool bo = false;
        CalculoClass cc = new CalculoClass();
        public Window1()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
               
                decimal num1 = 0;
                decimal num2 = 0;
                decimal resp = 0;
                bo = decimal.TryParse(num1TextBox.Text, out num1);
                bo = decimal.TryParse(num2TextBox.Text, out num2);
                switch (basicOperator)
                {
                    case 0:
                        resp = num1 + num2;
                        break;
                    case 1:
                        resp = num1 - num2;
                        break;
                    case 2:
                        resp = num1 * num2;
                        break;
                    case 3:
                        if (num2 == 0)
                        {
                            MessageBox.Show("No se puede efectuar una división entre 0");
                        }
                        else
                        {
                            resp = num1 / num2;
                        }
                        break;
                }
                ResultTextBox.Text = resp.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Se encontro una Exception"+ex);
            }
        }

        private void Checked_Handler(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            switch (rb.Name)
            {
                case "radioButtonSum":
                    basicOperator = 0;
                    break;
                case "radioButtonRest":
                    basicOperator = 1;
                    break;
                case "radioButtonMult":
                    basicOperator = 2;
                    break;
                case "radioButtonDiv":
                    basicOperator = 3;
                    break;
            }
        }

        private void cleanButton_Click(object sender, RoutedEventArgs e)
        {
            num1TextBox.Text = "";
            num2TextBox.Text = "";
            ResultTextBox.Text = "";
        }

        private void evaluateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                decimal numeva = 0;
               
                bo = decimal.TryParse(numEvTextBox.Text, out numeva);
                switch (specialOperator)
                {
                    case 0:
                        cc.getPrimo(numeva);
                       result2TextBox.Text = cc.getResp();
                        break;
                    case 1:
                        cc.getFactorial(numeva);
                        result2TextBox.Text = cc.getResult().ToString();
                        break;
                    case 2:
                       cc.getFibonacci(numeva);
                        result2TextBox.Text = cc.getResult().ToString();
                        break;
                    case 3:
                        cc.getSerieFibonacci(numeva);
                        result2TextBox.Text = cc.getResp();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Se encontro una Exception" + ex);
            }
        }

        private void Checked_Button(object sender, RoutedEventArgs e)
        {
            RadioButton rb2 = (RadioButton)sender;
            switch (rb2.Name)
            {
                case "radioButtonPrimo":
                    specialOperator = 0;
                    break;
                case "radioButtonFactorial":
                    specialOperator = 1;
                    break;
                case "radioButtonFibonacci":
                    specialOperator = 2;
                    break;
                case "radioButtonDiv":
                    specialOperator = 3;
                    break;
            }
        }

        private void clean2Button_Click(object sender, RoutedEventArgs e)
        {
            numEvTextBox.Text = "";
            result2TextBox.Text = "";
        }
    }

   
}
