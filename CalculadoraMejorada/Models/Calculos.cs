﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculadoraMejorada.Models
{
    class Calculos
    {
        double s = 1, u = 1, p, result, cont = 0;
        double i; 
        String series = "",Resp;
        public  double getFibonacci(double n)
        {
            switch (n)
            {
                case 0:
                    series = "0";
                    break;
                case 1:
                    series = "0, 1";
                    break;
                case 2:
                    series = "0, 1, 1";
                    break;
                default:
                    series = "0, 1, 1";
                    for (i = 3; i <= n; i++)
                    {
                        p = u;
                        u = s;
                        s = s + p;
                      
                    }
                    break;
        }
            return s;
        }

        public  String getFibonacciSeries(double n)
        {

            switch (n)
            {
                case 0:
                    series = "0";
                    break;
                case 1:
                    series = "0, 1";
                    break;
                case 2:
                    series = "0, 1, 1";
                    break;
                default:
                    series = "0, 1, 1";
                    for (i = 3; i <= n; i++)
                    {
                        p = u;
                        u = s;
                        s = s + p;
                        series += ", " + String.Concat(s);
                    }
                    break;
            }
            return series;
        }
     
        public  double getFactorial(double n)
        {
            double inicio = 1;
            for (double i = 1; i <= n; i++)
            {
                inicio = inicio * i;

            }
            result = inicio;
            return result;

        }
       
        public  String getIsPrimo(double n)
        {
            if (n==1)
            {
              Resp= "El número ingresado no es primo";
            }
            else
            {
                for (int i = 2; i < n; i++)
                {
                    if ((n % i) == 0)
                    {
                        cont++;
                    }
                }
                if (cont <= 2)
                {
                    Resp = "El número ingresado es primo";

                }
                else
                {
                    Resp = "El número ingresado no es primo";
                }
            }
            return Resp;
        }

        public double getS()
        {
            return s;
        }

        public double getFac()
        {
            return result;
        }

        public String getSerie()
        {
            return series;
        }
        public String getResp()
        {
            return Resp;
        }
    }
}
