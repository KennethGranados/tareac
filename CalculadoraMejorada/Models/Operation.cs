﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace CalculadoraMejorada.Models
{
    class Operation
    {
        double  resp;
        public void getResolver(int op, double valor1, double valor2)
        {
            switch (op)
            {
                case 1:
                    resp = valor1 + valor2;
                    break;
                case 2:
                    resp = valor1 - valor2;
                    break;
                case 3:
                    resp = valor1 * valor2;
                    break;
                case 4:
                    if (valor2 == 0)
                    {
                        MessageBox.Show("No se puede dividir entre 0");
                    }
                    else
                    {
                        resp = valor1 / valor2;
                    }
                    break;
               
            }
        }

   
        public double getResp()
        {
            return resp;
        }
    }
}
