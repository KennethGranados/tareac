﻿
using CalculadoraMejorada.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculadoraMejorada
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        double n1, n2;
        int ope=0;
        Calculos c = new Calculos();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Num0Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "0";
        }

        private void Num1Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "1";
        }

        private void Num2Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "2";
        }

        private void Num3Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "3";
        }

        private void Num4Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "4";
        }

        private void Num5Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "5";
        }

        private void Num6Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "6";
        }

        private void Num7Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "7";
        }

        private void Num8Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "8";
        }

        private void Num9Button_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = DisplayTextBox.Text + "9";
        }


        private void ResultButton_Click(object sender, RoutedEventArgs e)
        {
            n2 = Convert.ToDouble(DisplayTextBox.Text);
            Operation op = new Operation();
            op.getResolver(ope, n1, n2);
          
            DisplayTextBox.Text = op.getResp().ToString();
        }

        private void SumButton_Click(object sender, RoutedEventArgs e)
        {
            ope = 1;
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            DisplayTextBox.Text = "";
        }

        private void RestButton_Click(object sender, RoutedEventArgs e)
        {
            ope = 2;
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            DisplayTextBox.Text = "";
        }

        private void MultButton_Click(object sender, RoutedEventArgs e)
        {
            ope = 3;
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            DisplayTextBox.Text = "";
        }

        private void DivButton_Click(object sender, RoutedEventArgs e)
        {
            ope = 4;
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            DisplayTextBox.Text = "";
        }

        private void CButton_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text="";
            n2 = 0;
        }

        private void PrimoButton_Click(object sender, RoutedEventArgs e)
        {
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            c.getIsPrimo(n1);
            DisplayTextBox.Text = c.getResp();
        }

        private void FactButton_Click(object sender, RoutedEventArgs e)
        {
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            c.getFactorial(n1);
            DisplayTextBox.Text = c.getFac().ToString();
        }

        private void FibButton_Click(object sender, RoutedEventArgs e)
        {
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            c.getFibonacci(n1);
            DisplayTextBox.Text = c.getS().ToString();
        }

        private void SerieButton_Click(object sender, RoutedEventArgs e)
        {
            n1 = Convert.ToDouble(DisplayTextBox.Text);
            c.getFibonacciSeries(n1);
            DisplayTextBox.Text = c.getSerie();
        }

        private void CEButton_Click(object sender, RoutedEventArgs e)
        {
            DisplayTextBox.Text = "";
            n2 = 0;
            n1 = 0;
        }
    }   
}
